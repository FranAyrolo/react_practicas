import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
//action de redux
import { NuevoProductoAction } from "../actions/productoActions";
import { mostrarAlertaAction, ocultarAlertaAction } from "../actions/alertaActions";

const NuevoProducto = ({history}) => {

    const [nombre, guardarNombre] = useState("");
    const [precio, guardarPrecio] = useState(0);
    const dispatch = useDispatch();

    const cargando = useSelector(state => state.productos.loading);
    const error = useSelector(state => state.productos.error)
    const alerta = useSelector(state => state.alerta.alerta);

    //pasamano para finalmente llamar productoAction
    const agregarProducto = (producto) => dispatch( NuevoProductoAction(producto) );

    const submitNuevoProducto = e => {
        e.preventDefault();
        //validaciones y creacion
        if (nombre.trim === "" || precio <= 0) {
            const alertaProducto = {
                msg: "ambos cambios son obligatorios", 
                classes: "alert alert-danger text-center text-uppercase p3"
            }
            dispatch(mostrarAlertaAction(alertaProducto));
            return;
        }
        
        dispatch(ocultarAlertaAction());
        agregarProducto({
            nombre,
            precio
        });
        history.push("/");
    }

    return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Agregar nuevo Producto
                        </h2>

                        {alerta ? <p className={alerta.classes}>{alerta.msg}</p> : null}

                        <form
                            onSubmit={submitNuevoProducto}
                        >
                            <div className="form-group">
                                <label> Nombre Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => guardarNombre(e.target.value)}
                                />
                            </div>

                            <div className="form-group">
                                <label> Precio del Producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio del Producto"
                                    name="precio"
                                    value={precio}
                                    onChange={e => guardarPrecio(Number(e.target.value))}
                                />
                            </div>

                            <button 
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                            >
                            Agregar
                            </button>
                        </form>
                        { cargando ? <p>Cargando...</p> : null}
                        { error ? < p>Hubo un error</p> : null}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NuevoProducto;