import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { obtenerProductosAction } from "../actions/productoActions";
import Producto from "./Producto";

const Productos = () => {

    const dispatch = useDispatch();

    useEffect( () => {
        const cargarProductos = () => dispatch(obtenerProductosAction());
        cargarProductos();
    }, [dispatch]);

    const productos = useSelector( state => state.productos.productos);
    const cargando = useSelector(state => state.productos.loading);
    const error = useSelector(state => state.productos.error);

    return (
        <Fragment>
            <h2 className="text-center my-5"> Listado de Productos</h2>

            {error ? 
                <p className="font-weight-bold alert alert-danger text-center mt-4">Error al cargar los productos</p>
                :
                null
            }  

            {cargando ?
                <p>CARGANDO...</p>
                :
                null
            }

            <table className="table table-striped">
                <thead className="bg-primary table-dark">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {productos.length !== 0 ?
                        productos.map(producto => (
                            <Producto
                                key={producto.id}
                                producto={producto}
                            />
                        ))
                        :
                        null
                    }
                </tbody>
            </table>
        </Fragment>
    )
}

export default Productos;