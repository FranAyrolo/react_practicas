import React from "react";
import {useHistory} from "react-router-dom"
import { useDispatch } from "react-redux";
import { borrarProductoAction, seleccionarProductoEditarAction } from "../actions/productoActions";

const Producto = ({producto}) => {
    
    const {nombre, precio, id} = producto;
    const dispatch = useDispatch();
    const history = useHistory();

    const confirmarEliminar =id => {

        dispatch(borrarProductoAction(id));
    }

    const redireccionarEdicion = producto => {
        dispatch(seleccionarProductoEditarAction(producto));
        history.push(`/productos/editar/${producto.id}`);
    }

    return (
        <tr>
            <td>{nombre}</td>
            <td><span className="font-weight-bold">$ {precio}</span></td>
            <td className="acciones">
                <button
                    type="button"
                    onClick={() => redireccionarEdicion(producto)}
                    className="btn btn-primary mr-2">
                    Editar
                </button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => confirmarEliminar(id)}
                >Eliminar </button>
            </td>
        </tr>
    )
}

export default Producto;