import React, {useState, useEffect} from 'react';
import Pregunta from "./components/Pregunta.js";
import Formulario from "./components/Formulario.js";
import Listado from "./components/Listado.js";
import ControlPresupuesto from "./components/ControlPresupuesto.js";


function App() {
    
    const [presupuesto, setPresupuesto] = useState({
        inicial: 0,
        restante: 0
    });
    const [collapsePregunta, setCollapse] = useState(false);
    const [gastos, agregarGasto] = useState([]);
    
    useEffect (() => {
        
    }, []);
    
    const crearPresupuesto = (valor) => {
        setPresupuesto({
            inicial: valor,
            restante: valor
        });
    }
    
    const gastarPresupuesto = (gasto) => {
        agregarGasto([...gastos, gasto]);
        
        setPresupuesto({
            inicial: presupuesto.inicial,
            restante: presupuesto.restante - gasto.cantidad
        });
    }
    
    return (
    <div className="container">
    <header>
        <h1>Gasto Semanal</h1>

        <div className="contenido-principal contenido">
            {!collapsePregunta ?
            <Pregunta
                setPresupuesto={crearPresupuesto}
                collapsePregunta={setCollapse}
            />
            :
            <div className="row">
                <div className="one-half column">
                    <Formulario 
                        gastarPresupuesto={gastarPresupuesto}
                    />
                </div>
                
                <div className="one-half column">
                    <Listado 
                        gastos={gastos}
                    />
                    
                    <ControlPresupuesto
                        presupuesto={presupuesto.inicial}
                        restante={presupuesto.restante}
                    />
                </div>
            </div>
            }
        </div>
    </header>
    </div>
    );
}

export default App;
