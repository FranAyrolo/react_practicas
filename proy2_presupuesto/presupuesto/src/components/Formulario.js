import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Error from "./Error.js";
import shortid from 'shortid';

const Formulario = ({gastarPresupuesto}) => {
    
    const [nombre, setNombre] = useState('');
    const [cantidad, setCantidad] = useState(0);
    const [error, setError] = useState(false);
    
    const generarGasto = e => {
        e.preventDefault();
        
        if (cantidad < 1 || isNaN(cantidad) || nombre === '') {
            setError(true);
            return;
        }
        setError(false);
        
        const gasto = {
            nombre,
            cantidad,
            id: shortid.generate()
        }
        
        gastarPresupuesto(gasto);
        
        setNombre('');
        setCantidad(0);
    }
    
    return (
        <form
            onSubmit={generarGasto}
        >
            <h2>Agrega tus gastos aquí</h2>
            
            {error ? 
                <Error mensaje="Ambos campos deben ser correctos" />
                :
                null
            }
            
            <div className="campo">
                <label>Nombre </label>
                <input
                    type="text"
                    className="u-full-width"
                    placeholder="Ej. Transporte"
                    value={nombre}
                    onChange={e => setNombre(e.target.value)}
                />
            </div>
        
            <div className="campo">
                <label>Cantidad </label>
                <input
                    type="number"
                    className="u-full-width"
                    placeholder="Ej. 300"
                    value={cantidad}
                    onChange={e => setCantidad(parseInt(e.target.value))}
                />
            </div>
            
            <input
                type="submit"
                className="button-primary u-full-width"
                value="Agregar gasto"
            />
        
        </form>
    );

}

Formulario.propTypes = {
    gastarPresupuesto: PropTypes.func.isRequired
}

export default Formulario;
