import React, {useState} from 'react';
import {Link} from "react-router-dom";

const NuevaCuenta = () => {
    
    const [usuario, guardarUsuario] = useState({
        nombre: '',
        email: '',
        password: '',
        repetirPassword: ''
    });

    const {nombre, email, password, repetirPassword} = usuario;

    const onChange = (e) => {
        guardarUsuario({
            ...usuario, [e.target.name] : e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();

    }

    return (
        <div className="form-usuario">
            <div className="contenedor-form sombra-dark">
                <h1> Obtener una cuenta </h1>
                
                <form
                    onSubmit={onSubmit}
                >
                    <div className="campo-form">
                        <label htmlFor="Nombre"> Nombre de Usuario </label>
                        <input
                            type="text"
                            id="nombre"
                            name="nombre"
                            placeholder="username"
                            value={nombre}
                            onChange={onChange}
                            />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="email"> Email </label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="tu email"
                            value={email}
                            onChange={onChange}
                            />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="password"> Password </label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="contraseña"
                            value={password}
                            onChange={onChange}
                            />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="confirmar"> Confirmación </label>
                        <input
                            type="password"
                            id="repetirPassword"
                            name="repetirPassword"
                            placeholder="repite tu password"
                            value={repetirPassword}
                            onChange={onChange}
                            />
                    </div>

                    <div className="campo-form">
                        <input type="submit" className="btn btn-primario btn-block"
                            value="Iniciar Sesión" />
                    </div>
                </form>
                <Link to={"/"} className="enlace-cuenta">
                    Volver a inicio de sesión
                </Link>
            </div>
        </div>
    );

}

export default NuevaCuenta;
