import React, {useEffect, useContext} from 'react';
import Proyecto from "./Proyecto";
import proyectoContext from "../../context/proyectos/proyectoContext";

const Listado = () => {

    const {proyectos, obtenerProyectos} = useContext(proyectoContext);

    useEffect(()=> {
        obtenerProyectos();
    }, [])

    if(proyectos.length === 0) return <p>Aún no hay proyectos</p>;

    return (
        <ul className="listado-proyectos">
            {proyectos.map(proyecto => (
                <Proyecto
                    key={proyecto.id}
                    proyecto={proyecto}
                />
            ))}
        </ul>
    );
}

export default Listado;
