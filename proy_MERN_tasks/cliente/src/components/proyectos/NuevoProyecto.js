import React, {Fragment, useState, useContext} from 'react';
import proyectoContext from "../../context/proyectos/proyectoContext";

const NuevoProyecto = () => {
    
    const {formulario, errorFormulario, mostrarFormulario, agregarProyecto, mostrarError} = useContext(proyectoContext);

    const [proyecto, guardarProyecto] = useState({
        nombre: ''
    });

    const {nombre} = proyecto;

    const onChange = (e) => {
        guardarProyecto({
            ...proyecto, [e.target.name] : e.target.value
        })
    }

    const submitProyecto = (e) => {
        e.preventDefault();

        if (nombre === "") {
            mostrarError();
            return;
        }

        agregarProyecto(proyecto);
        guardarProyecto({
            nombre: ""
        })
    }

    return (
        <Fragment>
            <button
                className="btn btn-block btn-primario"    
                type="button"
                onClick={() => mostrarFormulario()}
            >Nuevo Proyecto</button>

            { formulario?
            (
                <form
                    className="formulario-nuevo-proyecto"
                    onSubmit={submitProyecto}
                >
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Nombre Proyecto"
                        name="nombre"
                        value={nombre}
                        onChange={onChange}
                    />

                    <input
                        type="submit"
                        className="btn btn-primario btn-block"
                        value="Agregar Proyecto"
                    />
                </form>
            )
            :
                null
            }

            { errorFormulario ?
                <p className="mensaje error">Debes ingresar un nombre</p>
            :
                null
            }
        </Fragment>
    );

}

export default NuevoProyecto;
