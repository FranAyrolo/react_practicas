import React, {useState, useContext, useEffect} from 'react';
import proyectoContext from "../../context/proyectos/proyectoContext";
import tareaContext from "../../context/tareas/tareaContext";

const FormTarea = () => {
    
    const {proyecto} = useContext(proyectoContext);
    const {tareaSeleccionada, agregarTarea, obtenerTareas, modificarTarea} = useContext(tareaContext);

    const [tarea, guardarTarea] = useState({
        nombre: ""
    })
    const {nombre} = tarea;

    useEffect (() => {
        if (tareaSeleccionada !== null) {
            guardarTarea(tareaSeleccionada)
        }
        else {
            guardarTarea({
                nombre: ""
            })
        }
    }, [tareaSeleccionada]);

    if (!proyecto) {
        return null;
    }

    const [proyectoActual] = proyecto;

    const handleChange = e => {
        guardarTarea({
            ...tarea, [e.target.name] : e.target.value
        })
    }

    const onSubmit = e => {
        e.preventDefault();
        if (nombre.trim() === "" ) {
            return;
        }

        if (!tareaSeleccionada) {
            tarea.proyectoId = proyectoActual.id;
            tarea.estado = false;
            agregarTarea(tarea);
        }
        else {
            modificarTarea(tarea);
        }
        
        guardarTarea({nombre :""});
        obtenerTareas(proyectoActual.id);
    }

    return (
        <div className="formulario">
            <form
                onSubmit={onSubmit}
            >
                <div className="contenedor-input">
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Nombre Tarea..."
                        name="nombre"
                        value={nombre}
                        onChange={handleChange}
                    />  
                </div>

                <div className="contenedor-input">
                    <input
                        type="submit"
                        className="btn btn-primario btn-submit btn-block"
                        value={tareaSeleccionada? "Editar Tarea" : "Agregar Tarea"}
                    />  
                </div>
            </form>
        </div>
    );

}

export default FormTarea;
