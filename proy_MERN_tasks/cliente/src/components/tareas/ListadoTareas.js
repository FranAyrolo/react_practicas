import React, {Fragment, useContext} from 'react';
import proyectoContext from "../../context/proyectos/proyectoContext";
import Tarea from './Tarea';
import tareaContext from "../../context/tareas/tareaContext";

const ListadoTareas = () => {
    
    const {proyecto, eliminarProyecto} = useContext(proyectoContext);
    const {tareasProyecto, eliminarTarea, modificarTarea, guardarTareaActual} = useContext(tareaContext);

    if (!proyecto) {
        return (<h2>Selecciona un proyecto</h2>)
    }
    const [proyectoActual] = proyecto;

    const onClickEliminar = () => {
        eliminarProyecto(proyectoActual.id)
    }

    const cambiarEstadoTarea = tarea => {
        tarea.estado = !tarea.estado;
        modificarTarea(tarea);
    }

    return (
        <Fragment>
            <h2>Proyecto: {proyectoActual.nombre}</h2>

            <ul  className="listado-tareas">
                {tareasProyecto.length === 0 ?
                    (<li className="tarea"><p>No hay tareas...</p></li>)
                    :
                    tareasProyecto.map(tarea => (
                        <Tarea
                            key={tarea.id}
                            tarea={tarea}
                            eliminarTarea={eliminarTarea}
                            cambiarEstado={cambiarEstadoTarea}
                            guardarTarea={guardarTareaActual}
                        />
                    ))
                }
            </ul>

        
            <button
                type="button"
                className="btn btn-eliminar"
                onClick={onClickEliminar}
            >Eliminar Proyecto &times;</button>
        </Fragment>
    );

}

export default ListadoTareas;
