import React from 'react';

const Tarea = ({tarea, eliminarTarea, cambiarEstado, guardarTarea}) => {
    return (
        <li className="tarea sombra">
            <p>{tarea.nombre}</p>

            <div className="estado">
                {tarea.estado ?
                    (
                    <button
                        type="button"
                        className="completo"
                        onClick={() => cambiarEstado(tarea)}
                        >Completo</button>
                    )
                :
                    (
                    <button
                        type="button"
                        className="incompleto"
                        onClick={() => cambiarEstado(tarea)}
                    >Incompleto</button>
                    )
                }
            </div>

            <div className="acciones">
                <button
                    type="button"
                    className="btn btn-primario"
                    onClick={() => guardarTarea(tarea)}
                >Editar</button>

                <button
                    type="button"
                    className="btn btn-primario"
                    onClick={() => eliminarTarea(tarea.id)}
                >Eliminar</button>
            </div>
        </li>
    );

}

export default Tarea;
