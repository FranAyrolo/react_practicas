import React, {useReducer} from "react";
import TareaContext from "./tareaContext";
import TareaReducer from "./tareaReducer";
import {TAREAS_PROYECTO, AGREGAR_TAREA, ELIMINAR_TAREA, TAREA_ACTUAL, EDITAR_TAREA} from "../../types/index";
import {v4 as uuid} from 'uuid';

const TareaState = props => {
    const initialState = {
        tareas: [
            {id: 1, nombre: "Elegir Plataforma", estado: true, proyectoId: 1},
            {id: 2, nombre: "Evaluar Costos", estado: false, proyectoId: 2},
            {id: 3, nombre: "Contemplar Ombligo", estado: true, proyectoId: 3},
            {id: 4, nombre: "Elegir Nombre =/= Mike", estado: false, proyectoId: 4},
            {id: 5, nombre: "Tarea A", estado: true, proyectoId: 1},
            {id: 6, nombre: "Tarea B", estado: false, proyectoId: 2},
            {id: 7, nombre: "Tarea C", estado: true, proyectoId: 3},
            {id: 8, nombre: "Tarea D", estado: false, proyectoId: 4}
        ],
        tareasProyecto: null,
        tareaSeleccionada: null
    }

    const [state, dispatch] = useReducer(TareaReducer, initialState);

    const obtenerTareas = proyectoId => {
        dispatch({
            type: TAREAS_PROYECTO,
            payload: proyectoId
        })
    }

    const agregarTarea = tarea => {
        tarea.id = uuid();
        dispatch ({
            type: AGREGAR_TAREA,
            payload: tarea
        })
    }

    const eliminarTarea = id => {
        dispatch ({
            type: ELIMINAR_TAREA,
            payload: id
        })
    }

    const modificarTarea = tarea => {
        dispatch ({
            type: EDITAR_TAREA,
            payload: tarea
        })
    }

    const guardarTareaActual = tarea => {
        dispatch ({
            type: TAREA_ACTUAL,
            payload: tarea
        })
    }

    return (
        <TareaContext.Provider
            value = {{
                tareas: state.tareas,
                tareasProyecto : state.tareasProyecto,
                tareaSeleccionada: state.tareaSeleccionada,
                obtenerTareas,
                agregarTarea,
                eliminarTarea,
                modificarTarea,
                guardarTareaActual
            }}
        >
            {props.children}
        </TareaContext.Provider>
    )
}

export default TareaState;