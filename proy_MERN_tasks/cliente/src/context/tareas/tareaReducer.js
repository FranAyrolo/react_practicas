import {AGREGAR_TAREA, TAREAS_PROYECTO, ELIMINAR_TAREA, TAREA_ACTUAL, EDITAR_TAREA} from "../../types/index";

const tareaReducer = (state, action) => {
    switch(action.type) {
        case TAREAS_PROYECTO:
            return {
                ...state,
                tareasProyecto: state.tareas.filter(tarea => tarea.proyectoId === action.payload)
            }

        case AGREGAR_TAREA:
            return {
                ...state, 
                tareas: [...state.tareas, action.payload]
            }

        case ELIMINAR_TAREA:
            return {
                ...state,
                tareas: state.tareas.filter(tarea => tarea.id !== action.payload),
                tareasProyecto: state.tareasProyecto.filter(tarea => tarea.id !== action.payload)
            }
        
        case EDITAR_TAREA:
            return {
                ...state,
                tareas: state.tareas.map(tarea => tarea.id === action.payload.id? action.payload : tarea),
                tareaSeleccionada: ""
            }

        case TAREA_ACTUAL:
            return {
                ...state,
                tareaSeleccionada: action.payload
            }

        default:
            return state;
    }
}

export default tareaReducer;