const { mongo } = require("mongoose");

const mongoose = require("mongoose");

const TareaSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    estado: {
        type: Boolean,
        required: true
    }
});

module.exports = mongoose.model("Tarea", TareaSchema);