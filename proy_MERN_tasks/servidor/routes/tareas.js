const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const {check} = require("express-validator");
const tareaController = require("../controllers/tareaController");

router.post("/",
    [
        check("nombre", "La tarea debe tener nombre").not().isEmpty()
    ],
    tareaController.crearTarea
);

router.get("/",
    tareaController.obtenerTareas
);

router.put("/",//falta id
    tareaController.actualizarTarea
);

router.delete("/",
    tareaController.eliminarTarea
);

module.exports = router;