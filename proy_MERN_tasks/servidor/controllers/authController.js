const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const {validationResult} = require("express-validator");
const jwt = require("jsonwebtoken");

exports.autenticarUsuario = async(req, res) => {
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({errores: errores.array()});
    }

    const {email, password} = req.body;

    try {
        let usuario = await Usuario.findOne({email});
        if (!usuario) {
            return res.status(400).json({msg: "El usuario no existe"});
        }

        const passVerif = await bcryptjs.compare(password, usuario.password);
        if (!passVerif) {
            return res.status(400).json({msg: "contraseña incorrecta"});
        }

        //Crear y firmar web token
        const payload = {
            usuario: {
                id: usuario.id
            }
        }

        jwt.sign(payload, process.env.AUTHWORD, {
            expiresIn: 3600
        }, (error, token) => {
            if (error) throw error;
            res.json({token});
        });

    } catch (error) {
        console.log(error);
    }
}