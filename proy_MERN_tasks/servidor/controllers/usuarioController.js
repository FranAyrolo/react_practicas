const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const {validationResult} = require("express-validator");
const jwt = require("jsonwebtoken");

exports.crearUsuario = async(req, res) => {
    const {password} = req.body;
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({errores: errores.array()});
    }

    try {
        let usuario;
        usuario = new Usuario(req.body);

        const salt = await bcryptjs.genSalt(10);
        usuario.password = await bcryptjs.hash(password, salt);
        
        await usuario.save();

        //Crear y firmar web token
        const payload = {
            usuario: {
                id: usuario.id
            }
        }

        jwt.sign(payload, process.env.AUTHWORD, {
            expiresIn: 3600
        }, (error, token) => {
            if (error) throw error;
            res.json({token});
        });

        //res.send("usuario creado correctamente");
    } catch (error) {
        if (error.name =="MongoError" && error.code == 11000){
            return res.status(422).send({
                success: false,
                message: "Ese mail ya está en uso"
            });
        }
        console.log(error);
        res.status(400).send("hubo un error");
    }
    
    console.log(req.body);
}