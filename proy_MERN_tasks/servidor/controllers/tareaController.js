const Tarea = require("../models/Tarea");
const {validationResult} = require("express-validator");

//POST
exports.crearTarea = async (req, res) => {
    console.log("creacion de una tarea");
    const tarea = new Tarea(req.body);
    tarea.save();
    res.json(tarea);
}

//GET
exports.obtenerTareas = async (req, res) => {
    console.log("obtener tareas de un proyecto");
}

//PUT
exports.actualizarTarea = async (req, res) => {
    console.log("actualización de tarea");
}

//DELETE
exports.eliminarTarea = async (req, res) => {
    console.log("eliminación de una tarea");
}