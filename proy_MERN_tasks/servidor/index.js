//imports
const express = require("express");
const conectarDB = require("./config/db");



//crear el servidor
const app = express();

conectarDB();
app.use(express.json({extend: true}));

//puerto de la app
const PORT = process.env.PORT || 4000;

app.use("/api/usuarios", require("./routes/usuarios"));
app.use("/api/auth", require("./routes/auth"));
app.use("/api/proyectos", require("./routes/proyectos"));
app.use("/api/tareas", require("./routes/tareas"));

app.get("/", (req, res) => {
    res.send("Hello World")
});

//correr la app
app.listen(PORT, () => {
    console.log(`El servidor está funcionando, en el puerto ${PORT}`)
});