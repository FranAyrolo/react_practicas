import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
const shortid = require('shortid');


const Formulario = ({crearCita}) => {
    
    const [cita, actualizarCita] = useState({
        mascota: '',
        dueño: '',
        fecha: '',
        hora: '',
        sintomas: ''
    });
    
    const [error, actualizarError] = useState(false);
    
    //actualiza la cita actual a medida que cambia
    const updateState = e => {
        actualizarCita({...cita, [e.target.name] : e.target.value})
    }
    
    //verifica y submitea la cita
    const submitCita = (e) => {
        e.preventDefault();
       
        for (const item of Object.values(cita)) {
            if (item.trim() === '') {
               console.log("error, hay un elemento vacio");
               actualizarError(true);
               return;
            }
        }
       
        actualizarError(false);
        cita.id = shortid.generate();
        
        //crearCita es de App.js
        crearCita(cita);
        
        actualizarCita({
            mascota: '',
            dueño: '',
            fecha: '',
            hora: '',
            sintomas: ''
        })
    } 
    
    return (
        <Fragment>
            <h2>Crear Cita</h2>
            
            {error ?
                <p className="alerta-error">Todos los campos son obligatorios</p>
                :
                null
            }
            
            <form
                onSubmit={submitCita}
            >
                <label>Nombre Mascota</label>
                <input
                    type="text"
                    name="mascota"
                    className="u-full-width"
                    placeholder="Nombre Mascota"
                    onChange={updateState}
                    value={cita.mascota}
                />
                
                <label>Nombre Dueño</label>
                <input
                    type="text"
                    name="dueño"
                    className="u-full-width"
                    placeholder="Nombre Dueño"
                    onChange={updateState}
                    value={cita.dueño}
                />
                
                <label>Fecha</label>
                <input
                    type="date"
                    name="fecha"
                    className="u-full-width"
                    onChange={updateState}
                    value={cita.fecha}
                />
                
                <label>Hora</label>
                <input
                    type="time"
                    name="hora"
                    className="u-full-width"
                    onChange={updateState}
                    value={cita.hora}
                />
                
                <label>Síntomas</label>
                <textarea
                    className="u-full-width"
                    name="sintomas"
                    onChange={updateState}
                    value={cita.sintomas}
                ></textarea>
                
                <button
                    type="submit"
                    className="u-full-width button-primary"
                >Agregar Cita</button>
            </form>
        </Fragment>
    );

}

Formulario.propTypes = {
    crearCita: PropTypes.func.isRequired
}


export default Formulario;
