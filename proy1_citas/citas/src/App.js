import React, {Fragment, useState, useEffect} from 'react';
import Formulario from './components/Formulario.js';
import Cita from './components/Cita.js';

function App() {
    
    let citasIniciales = JSON.parse(localStorage.getItem('citas'));
    if (!citasIniciales) {
        citasIniciales =[];
    }
    
    const [citas, updateCitas] = useState(citasIniciales);
    
    useEffect( () => {
        if(citasIniciales) {
            localStorage.setItem('citas', JSON.stringify(citas));
        } else {
            localStorage.setItem('citas', JSON.stringify([]));
        }
    }, [citas, citasIniciales]);
    
    const crearCita = cita => {
        updateCitas([...citas, cita])
    }
    
    const eliminarCita = id => {
        const nueva = citas.filter(cita => cita.id !== id)
        updateCitas(nueva);
    }
    
    const mensajeListado = citas.length === 0 ? "No hay citas aún"
        :
        "Administrar Citas"
    
    //podría pasarle a Cita la lista de citas en lugar de hacer un map?
  return (
    <Fragment>
        <h1>Administrador de pacientes</h1>
        
        <div className="container">
            <div className="row">
                <div className="one-half column">
                    <Formulario
                        crearCita={crearCita}
                     />
                </div>
                <div className="one-half column">
                    <h2>{mensajeListado}</h2>
                    {citas.map(cita => (
                        <Cita 
                            key={cita.id}
                            cita={cita}
                            eliminarCita={eliminarCita}
                        />
                    ))}
                    
                </div>
            </div>
        </div>
        
    </Fragment>
  );
}

export default App;
