import React, {useState} from 'react';
import Frase from './components/Frase.js';

function App() {
    
    const [frase, getFrase] = useState([]);
    
    const consultAPI = async() => {
        const APIresponse = await fetch("https://breaking-bad-quotes.herokuapp.com/v1/quotes");
        const frase = await APIresponse.json()
        getFrase(frase[0]);
    }
    
    return (
    <div className="App">
        <contenedor>
            <Frase
                frase={frase}
            />
        
            <button
                onClick={consultAPI}
            >
                Breaking DOU
            </button>
        </contenedor>
    </div>
    );
}

export default App;
